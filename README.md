# SENTO Project #

Kurze Zusammenfassung der bisher entwickelten Elemente.

## Setup ##

### Libraries ###
* Installation von AdaFruit NeoPixel Library ()
* Installation von AdaFruit BlueFruitLE Library (https://github.com/adafruit/Adafruit_BluefruitLE_nRF51)

### Aktuelle PIN Belegung Einstellung ###

* AdaFruit Ring: 6 (kollidiert aktuell mit SPI RESET)
* AdaFruit Pixel (Icons): 12
* AdaFruit Bluetooth: 
    * SPI CS:  8
    * SPI IRQ: 7
    * SPI RST: 6
* DEBUG LED Pin: 13

