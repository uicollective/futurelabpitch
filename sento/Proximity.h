#ifndef _PROXY_H_
#define _PROXY_H_

void Proxy_Init();
void Proxy_Tick();

/*! returns a nonzero number if an object is close to the sensor, 0 otherwise or in case the value could not be read. */
uint16_t Proxy_GetDistance();

/*! returns a nonzero number for one tick if a swipe was detected. In this case, the number is the closest proximity during swipe. */
uint16_t Proxy_IsSwipe();

#endif

