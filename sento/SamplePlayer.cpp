
#include "SamplePlayer.h"

#define WAIT_TC16_REGS_SYNC(x) while(x->COUNT16.STATUS.bit.SYNCBUSY) {}

uint32_t toneMaxFrequency = F_CPU / 2;
uint32_t lastOutputPin = 0xFFFFFFFF;


volatile static bool sample_inited = false;
volatile static const uint8_t* sample_base = NULL;
volatile static uint32_t sample_idx = 0;
volatile static uint32_t sample_count = 0;
volatile static bool sample_loop = false;

#define SAMPLE_TC         TC4
#define SAMPLE_TC_IRQn    TC4_IRQn
#define SAMPLE_TC_TOP     0xFFFF
#define SAMPLE_TC_CHANNEL 0
#define SAMPLE_HANDLER    TC4_Handler
#define SAMPLE_PIN        PIN_A0
#define SAMPLE_SHIFT      0
#define SAMPLE_DOWNSHIFT  0

void Sample_Init() {
  if(!sample_inited) {
    //set prio to high  
    NVIC_SetPriority(SAMPLE_TC_IRQn, 0);
    // Enable GCLK for TC4 and TC5
    GCLK->CLKCTRL.reg = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID(GCM_TC4_TC5));
    while (GCLK->STATUS.bit.SYNCBUSY);
    analogWrite(SAMPLE_PIN, (0x80 << SAMPLE_SHIFT) >> SAMPLE_DOWNSHIFT);
    sample_inited = true;  
  }  
}

void Sample_Play(const uint8_t* samples, uint32_t numSamples, uint32_t frequency, bool loop) {
  
  //reset everything
  Sample_Init();
  Sample_Stop();

  //remember values
  sample_base = samples;
  sample_idx = 0;
  sample_count = numSamples;
  sample_loop = loop;
  
  //find divider / determine counter configuration
  uint32_t matchValue = (F_CPU >> 1) / frequency - 1;
  uint32_t prescale = (matchValue / 0xffff) + 1;
  uint32_t ctrla = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_WAVEGEN_MFRQ;

  if (prescale > 256) {
    prescale = 1024;
    ctrla |= TC_CTRLA_PRESCALER_DIV1024;
    matchValue >>= 10;
  } else if (prescale > 64) {
    prescale = 256;
    ctrla |= TC_CTRLA_PRESCALER_DIV256;
    matchValue >>= 8;
  } else if (prescale > 16) {
    prescale = 64;
    ctrla |= TC_CTRLA_PRESCALER_DIV64;
    matchValue >>= 6;
  } else if (prescale > 8) {
    prescale = 16;
    ctrla |= TC_CTRLA_PRESCALER_DIV16;
    matchValue >>= 4;
  } else if (prescale > 4) {
    prescale = 8;
    ctrla |= TC_CTRLA_PRESCALER_DIV8;
    matchValue >>= 3;
  } else if (prescale > 2) {
    prescale = 4;
    ctrla |= TC_CTRLA_PRESCALER_DIV4;
    matchValue >>= 2;
  } else if (prescale > 1) {
    prescale = 2;
    ctrla |= TC_CTRLA_PRESCALER_DIV2;
    matchValue >>= 1;
  } else {
    prescale = 1;
    ctrla |= TC_CTRLA_PRESCALER_DIV1;
  }    

  //configure timer
  SAMPLE_TC->COUNT16.CTRLA.reg |= ctrla;
  WAIT_TC16_REGS_SYNC(SAMPLE_TC)

  SAMPLE_TC->COUNT16.CC[SAMPLE_TC_CHANNEL].reg = (uint16_t) matchValue;
  WAIT_TC16_REGS_SYNC(SAMPLE_TC)

  // Enable timer interrupt request
  SAMPLE_TC->COUNT16.INTENSET.bit.MC0 = 1;
  
  // start timer/counter
  SAMPLE_TC->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;
  WAIT_TC16_REGS_SYNC(SAMPLE_TC)
  
  // Enable interrupt
  NVIC_EnableIRQ(SAMPLE_TC_IRQn);
}

void Sample_Stop() {
  // Disable Timer/Counter
  SAMPLE_TC->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  WAIT_TC16_REGS_SYNC(SAMPLE_TC)

  // Reset Timer/Counter
  SAMPLE_TC->COUNT16.CTRLA.reg = TC_CTRLA_SWRST;
  WAIT_TC16_REGS_SYNC(SAMPLE_TC)
  while (SAMPLE_TC->COUNT16.CTRLA.bit.SWRST);

  //Disable interrupt
  NVIC_DisableIRQ(SAMPLE_TC_IRQn);
  NVIC_ClearPendingIRQ(SAMPLE_TC_IRQn);
}

#ifdef __cplusplus
extern "C" {
#endif

void SAMPLE_HANDLER (void) {
  //clear interrupt
  SAMPLE_TC->COUNT16.INTFLAG.bit.MC0 = 1;

  if ((sample_idx >= sample_count) && sample_loop) {
    sample_idx = 0;
  }
  if (sample_idx >= sample_count) {
    Sample_Stop();
  } else {
    analogWrite(SAMPLE_PIN, (sample_base[sample_idx] << SAMPLE_SHIFT) >> SAMPLE_DOWNSHIFT);
    sample_idx++;
  }
}

#ifdef __cplusplus
}
#endif

