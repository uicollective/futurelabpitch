#include "SentoMode.h"
#include "constants.h"
#include "SamplePlayer.h"

#define TICKS_PER_STEP 30
#define FADE_DURATION 30

ShutdownMode::ShutdownMode() {}

void ShutdownMode::start() {
  Sample_Play(shutdownChimeSamples, shutdownChimeSampleCount, shutdownChimeSampleRate, false);
}



void ShutdownMode::tick(uint32_t ticks) {
  uint8_t currentStep = ticks / TICKS_PER_STEP;
  uint8_t ticksInStep = ticks % TICKS_PER_STEP;

  if (ticksInStep == 1) {
    switch (currentStep) {
      case 0:
        Icons_Fade(Icon_AirQuality,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        Icons_Fade(Icon_Brightness,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        Icons_Fade(Icon_Humidity,    ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        Icons_Fade(Icon_Temperature, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        LED_FadeAll(RING_HIGHLIGHT[0], RING_HIGHLIGHT[1], RING_HIGHLIGHT[2], RING_HIGHLIGHT[3], FADE_DURATION);
        break;
      case 1:
        Icons_Fade(Icon_AirQuality,  0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Brightness,  0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Humidity,    0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Temperature, 0, 0, 0, FADE_DURATION);
        LED_FadeAll(0, 0, 0, 0, FADE_DURATION);
        break;
    }
  }
}

