#include <Wire.h>

#include "Proximity.h"

#define ADDRESS         (0x80 >> 1) // 7 highest bits
#define DISTANCE_ADDR   0x5E

#define IN_RANGE_MAX 4000 /* actually, it's 4095 but we skip the last values */
#define SWIPE_MIN_TICKS 3
#define SWIPE_MAX_TICKS 30

static uint16_t distance = 0;
static uint16_t swipe = 0;

static uint16_t inRangeTicks = 0;
static uint16_t inRangeMin = 0;

void Proxy_Init() {
  Serial.println("initializing proximity sensor");
  Wire.begin();
  Serial.println("Proximity sensor initialized");
}

void Proxy_Tick() {
  swipe = 0;  //assume no swipe
  
  // Read basic measurement
  Wire.beginTransmission(ADDRESS);
  Wire.write(byte(DISTANCE_ADDR));
  Wire.endTransmission();

  Wire.requestFrom(ADDRESS, 2);
  if (Wire.available() >= 2) {
    uint8_t major = Wire.read();
    uint8_t minor = Wire.read();
    distance = (major << 4) | (minor & 0x0f);
  } else {
//    Serial.println("Could not read distance");
    distance = 0;
  }
  if (distance < IN_RANGE_MAX) {
    inRangeTicks++;
    if ((inRangeMin == 0) || (inRangeMin > distance)) {
      inRangeMin = distance;
    }
  } else {  //not in range
    if ((inRangeMin > 0) && (inRangeTicks >= SWIPE_MIN_TICKS) && (inRangeTicks <= SWIPE_MAX_TICKS)) { //swipe
      swipe = inRangeMin;
    }
    inRangeMin = 0;
    inRangeTicks = 0;
    distance = 0;
  }
}

uint16_t Proxy_GetDistance() {
  return distance;
}

uint16_t Proxy_IsSwipe() {
  return swipe;
}



