#include "SentoMode.h"

//idle mode

#define BREATHE_IN_DURATION 150
#define BREATHE_OUT_DURATION 150
#define BREATHE_REST_DURATION 50

IdleMode::IdleMode() {}

void IdleMode::start() {
  Icons_Fade(Icon_Humidity,    ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION);
  Icons_Fade(Icon_AirQuality,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION);
  Icons_Fade(Icon_Brightness,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION);
  Icons_Fade(Icon_Temperature, ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION);
  Serial.println("Starting Idle Mode");
}

void IdleMode::tick(uint32_t ticks) {
  uint16_t phase = ticks % (BREATHE_IN_DURATION + BREATHE_OUT_DURATION + BREATHE_REST_DURATION);
  if (phase == 1) { //start of breathe out duration
    LED_FadeAll(0, 0, 0, 80, BREATHE_OUT_DURATION);
  }
  if (phase == BREATHE_OUT_DURATION + BREATHE_REST_DURATION + 1) {  //start of breathe in
    LED_FadeAll(0, 0, 0, 160, BREATHE_IN_DURATION);
  }
}

