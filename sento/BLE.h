#ifndef _BLE_H_
#define _BLE_H_

typedef enum _BLEEvent {
  BLEEvent_None = 0,
  BLEEvent_Idle = 1,
  BLEEvent_TempChange = 2,
  BLEEvent_OpenWindow = 3,
  BLEEvent_DuskDawn = 4,
  BLEEvent_GuestAtHome = 5,
  BLEEvent_Resolved = 6,
  BLEEvent_OnOff = 7
} BLEEvent;

void BLE_Init();
BLEEvent BLE_Tick();

#endif
