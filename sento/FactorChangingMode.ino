#include "SentoMode.h"

//factor change mode: a factor is changing but nothing to be done

#define BREATHE_IN_DURATION 75
#define BREATHE_OUT_DURATION 50

FactorChangeMode::FactorChangeMode(bool inAirChange, bool inBrightChange, bool inHumidityChange, bool inTempChange) {
 airChange = inAirChange;
 brightChange = inBrightChange;
 humidityChange = inHumidityChange;
 tempChange = inTempChange;
}

void FactorChangeMode::start() {  
  if (!humidityChange) { Icons_Fade(Icon_Humidity,    0, 0, 0, BREATHE_IN_DURATION); }
  if (!airChange) { Icons_Fade(Icon_AirQuality,  0, 0, 0, BREATHE_IN_DURATION); }
  if (!brightChange) { Icons_Fade(Icon_Brightness,  0, 0, 0, BREATHE_IN_DURATION); }
  if (!tempChange) { Icons_Fade(Icon_Temperature, 0, 0, 0, BREATHE_IN_DURATION); }
}

void FactorChangeMode::tick(uint32_t ticks) {
  uint16_t phase = ticks % (BREATHE_IN_DURATION + BREATHE_OUT_DURATION);
  if (phase == 1) { //start of breathe out duration
    LED_FadeAll(0, 0, 0, 20, BREATHE_OUT_DURATION);
    if (humidityChange) { Icons_Fade(Icon_Humidity,    0, 0, 0, BREATHE_OUT_DURATION); }
    if (airChange) { Icons_Fade(Icon_AirQuality,       0, 0, 0, BREATHE_OUT_DURATION); }
    if (brightChange) { Icons_Fade(Icon_Brightness,    0, 0, 0, BREATHE_OUT_DURATION); }
    if (tempChange) { Icons_Fade(Icon_Temperature,     0, 0, 0, BREATHE_OUT_DURATION); }

  }
  if (phase == BREATHE_OUT_DURATION + 1) {  //start of breathe in
    LED_FadeAll(0, 0, 0, 255, BREATHE_IN_DURATION);
    if (humidityChange) { Icons_Fade(Icon_Humidity,    ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
    if (airChange) { Icons_Fade(Icon_AirQuality,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
    if (brightChange) { Icons_Fade(Icon_Brightness,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
    if (tempChange) { Icons_Fade(Icon_Temperature, ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }

  }
}

