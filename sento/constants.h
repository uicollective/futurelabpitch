#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

//#define OLD_SOUNDS

extern const uint8_t led_gamma[];

extern const uint32_t startupChimeSampleRate;
extern const uint32_t startupChimeSampleCount;
extern const uint8_t startupChimeSamples[];

extern const uint32_t shutdownChimeSampleRate;
extern const uint32_t shutdownChimeSampleCount;
extern const uint8_t shutdownChimeSamples[];


#endif
