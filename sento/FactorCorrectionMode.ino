#include "SentoMode.h"

//factor correction mode: factors causing a corrective action

#define BREATHE_IN_DURATION 75
#define BREATHE_OUT_DURATION 50

#define CORRECT_BREATH 5

FactorCorrectionMode::FactorCorrectionMode(bool inAirCause, bool inBrightCause, bool inHumidityCause, bool inTempCause, bool inAirCorr, bool inBrightCorr, bool inHumidityCorr, bool inTempCorr) {
 airCause = inAirCause;
 brightCause = inBrightCause;
 humidityCause = inHumidityCause;
 tempCause = inTempCause;
 airCorr = inAirCorr;
 brightCorr = inBrightCorr;
 humidityCorr = inHumidityCorr;
 tempCorr = inTempCorr;
}

void FactorCorrectionMode::start() {  
  if (!humidityCause) { Icons_Fade(Icon_Humidity,    ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
  if (!airCause) { Icons_Fade(Icon_AirQuality,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
  if (!brightCause) { Icons_Fade(Icon_Brightness,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
  if (!tempCause) { Icons_Fade(Icon_Temperature, ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, BREATHE_IN_DURATION); }
}

void FactorCorrectionMode::tick(uint32_t ticks) {
  uint16_t phase = ticks % (BREATHE_IN_DURATION + BREATHE_OUT_DURATION);
  uint16_t breatheCount = ticks / (BREATHE_IN_DURATION + BREATHE_OUT_DURATION);
  bool corr = (breatheCount > CORRECT_BREATH);
  
  bool humidityBlink = humidityCause || (humidityCorr && corr);
  bool airBlink = airCause || (airCorr && corr);
  bool brightBlink = brightCause || (brightCorr && corr);
  bool tempBlink = tempCause || (tempCorr && corr);
  
  if (phase == 1) { //start of breathe out duration
    LED_FadeAll(0, 0, 0, 0, BREATHE_OUT_DURATION);
    if (humidityBlink) { Icons_Fade(Icon_Humidity,    0, 0, 0, BREATHE_OUT_DURATION); }
    if (airBlink) { Icons_Fade(Icon_AirQuality,       0, 0, 0, BREATHE_OUT_DURATION); }
    if (brightBlink) { Icons_Fade(Icon_Brightness,    0, 0, 0, BREATHE_OUT_DURATION); }
    if (tempBlink) { Icons_Fade(Icon_Temperature,     0, 0, 0, BREATHE_OUT_DURATION); }
  }
  if (phase == BREATHE_OUT_DURATION + 1) {  //start of breathe in
    LED_FadeAll(0, 0, 255, 0, BREATHE_IN_DURATION);
    if (humidityBlink) { Icons_Fade(Icon_Humidity,    0, 0, 255, BREATHE_IN_DURATION); }
    if (airBlink) { Icons_Fade(Icon_AirQuality,  0, 0, 255, BREATHE_IN_DURATION); }
    if (brightBlink) { Icons_Fade(Icon_Brightness,  0, 0, 255, BREATHE_IN_DURATION); }
    if (tempBlink) { Icons_Fade(Icon_Temperature, 0, 0, 255, BREATHE_IN_DURATION); }

  }
}

