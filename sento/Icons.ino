#include "Icons.h"

#include "constants.h"

Adafruit_NeoPixel icons = Adafruit_NeoPixel(NUM_ICONS, NEO_ICONS_PIN, NEO_GRB + NEO_KHZ800); 
uint8_t iconBuffer[NUM_ICONS][3]; 

uint8_t iconFadeFrom[NUM_ICONS][3];  // fading: start RGB values of Icons
uint8_t iconFadeTo[NUM_ICONS][3];    // fading: end RGB values of Icons
uint8_t iconFadeDuration[NUM_ICONS]; // fading: animation duration in ticks (0 if not fading)
uint8_t iconFadeTime[NUM_ICONS];     // fading: current time of animation in ticks (0..ledFadeDuration)

void Icons_Init() {
  icons.begin(); 
  icons.show(); // switching all off
}

void Icons_Tick() {
  // service Icons - perform fading, copy to LEDs
  for(int i = 0; i<NUM_ICONS; i++){
    //check fading
    uint8_t duration = iconFadeDuration[i];
    if (duration) {
      uint8_t fadeTime = iconFadeTime[i];
      if (fadeTime > duration) {
        fadeTime = duration;
      }
      for (uint8_t comp = 0; comp < 3; comp++) {  //do linear interpolation for r,g,b
        uint16_t mix = (iconFadeTo[i][comp] * fadeTime + (duration-fadeTime) * iconFadeFrom[i][comp]);
        iconBuffer[i][comp] = mix / duration;
      }
      if (fadeTime >= duration) { //check animation end
        if (i == 0) { Serial.println("Fading end"); }
        iconFadeDuration[i] = 0;
      } else {
        iconFadeTime[i] = fadeTime + 1;
      }
    }
    icons.setPixelColor(i, led_gamma[iconBuffer[i][0]], led_gamma[iconBuffer[i][1]], led_gamma[iconBuffer[i][2]]); 
  }
  icons.show(); 
}

void Icons_Set(Icon icon, uint8_t r, uint8_t g, uint8_t b) {
  iconBuffer[icon][0] = r; 
  iconBuffer[icon][1] = g;
  iconBuffer[icon][2] = b;
  iconFadeTime[icon] = 0; //stop fading if running
}

void Icons_Fade(Icon icon, uint8_t r, uint8_t g, uint8_t b, uint8_t ticks) {
  if (ticks < 1) {  //no animation at all
    Icons_Set(icon, r, g, b);
  } else {
    iconFadeFrom[icon][0] = iconBuffer[icon][0];  //remember current color as start color
    iconFadeFrom[icon][1] = iconBuffer[icon][1];
    iconFadeFrom[icon][2] = iconBuffer[icon][2];
    iconFadeTo[icon][0] = r;
    iconFadeTo[icon][1] = g;
    iconFadeTo[icon][2] = b;
    iconFadeDuration[icon] = ticks;
    iconFadeTime[icon] = 0;
  }
}
