
#include "BLE.h"

#define BLUEFRUIT_SPI_CS               8
#define BLUEFRUIT_SPI_IRQ              7
#define BLUEFRUIT_SPI_RST              -1 //6    // Optional but recommended, set to -1 if unused

#include <Arduino.h>
#include <SPI.h>

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"

//BLE object using hardware SPI
Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

volatile BLEEvent newEvent;

int32_t command_characteristic_id;

// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}

void connected(void) {
  Serial.println( F("BLE Connected") );
}

void disconnected(void) {
  Serial.println( F("BLE Disconnected") );
}

void BleGattRX(int32_t chars_id, uint8_t data[], uint16_t len) {
  
  if (chars_id == command_characteristic_id) {
    Serial.write(data, len);
    Serial.println();
    newEvent = (BLEEvent)data[0];
  }
}

void BLE_Init(void) {
  Serial.println( F("Initializing BLE") );
  
  if (!ble.begin(true)) { //start in verbose mode
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }

  // Perform a factory reset to make sure everything is in a known state
  Serial.println(F("Performing a factory reset: "));
  if ( ! ble.factoryReset() ){
    error(F("Couldn't factory reset"));
  }
  
  if ( !ble.isVersionAtLeast("0.7.0") ) {
    error( F("Need at least BLE firmware 0.7.0. Please update using Adafruit Bluefruit LE Connect app") );
  }

  if (! ble.sendCommandCheckOK(F("AT+GAPDEVNAME=Sento")) ) {
    error(F("Could not set device name?"));
  }

  //add service and characteristic: FDCE9817-127E-4520-98C2-78C217D66E0A
  ble.sendCommandCheckOK( F("AT+GATTADDSERVICE=uuid128=FD-CE-98-17-12-7E-45-20-98-C2-78-C2-17-D6-6E-0A") );
  //ble.sendCommandCheckOK( F("AT+GATTADDSERVICE=uuid=0x1234") );
  ble.sendCommandWithIntReply( F("AT+GATTADDCHAR=UUID=0x2345,PROPERTIES=0x08,MIN_LEN=1,MAX_LEN=6,DATATYPE=string,VALUE=abc"), &command_characteristic_id);

  //reset needed after service change
  ble.reset();
  ble.echo(false);

  // Set callbacks
  ble.setConnectCallback(connected);
  ble.setDisconnectCallback(disconnected);
  ble.setBleGattRxCallback(command_characteristic_id, BleGattRX);

  Serial.println( F("BLE init done.") );

}

BLEEvent BLE_Tick(void) {  
  ble.update(200);
  BLEEvent currentEvent = newEvent;
  newEvent = BLEEvent_None;
  return currentEvent;
}

