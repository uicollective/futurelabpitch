#include "SentoMode.h"
#include "constants.h"
#include "SamplePlayer.h"

#define TICKS_PER_STEP 50
#define FADE_DURATION 40

StartMode::StartMode(): done(false) {
  done = false; 
}

void StartMode::start() {
  LED_SetAll(0, 0, 0, 0);

  // Set Icons 
  Icons_Set(Icon_Humidity, 0, 0, 0); 
  Icons_Set(Icon_AirQuality, 0, 0, 0); 
  Icons_Set(Icon_Brightness, 0, 0, 0); 
  Icons_Set(Icon_Temperature, 0, 0, 0); 
  
  done = false;
  Serial.println("Starting Start Mode");

  Sample_Play(startupChimeSamples, startupChimeSampleCount, 30000, false);
  }



void StartMode::tick(uint32_t ticks) {
  uint8_t currentStep = ticks / TICKS_PER_STEP;
  uint8_t ticksInStep = ticks % TICKS_PER_STEP;

  if (ticksInStep == 1) {
    Serial.println("next step");
//    currentStep = 7;  //uncomment to skip startup animation
    switch (currentStep) {
      case 0:
        Serial.println("starting ring");
        LED_FadeAll(RING_HIGHLIGHT[0], RING_HIGHLIGHT[1], RING_HIGHLIGHT[2], RING_HIGHLIGHT[3], FADE_DURATION);
        break;
      case 1:
        Serial.println("starting air quality");
        Icons_Fade(Icon_AirQuality,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 2:
        Icons_Fade(Icon_Brightness,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 3:
        Icons_Fade(Icon_Humidity,    ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 4:
        Icons_Fade(Icon_Temperature, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 6:
        Icons_Fade(Icon_AirQuality,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, FADE_DURATION);
        Icons_Fade(Icon_Brightness,  ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, FADE_DURATION);
        Icons_Fade(Icon_Humidity,    ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, FADE_DURATION);
        Icons_Fade(Icon_Temperature, ICON_WHITE_R, ICON_WHITE_G, ICON_WHITE_B, FADE_DURATION);
        LED_FadeAll(0, 0, 0, 255, FADE_DURATION);
        break;
      case 7:
        done = true;
        setMode(&idleMode);
        break;
    }
  }
}

bool StartMode::allowsModeChange() {
  return done;
}

