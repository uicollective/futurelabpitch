
#pragma once

#ifdef __cplusplus

#include "Arduino.h"

void Sample_Play(const uint8_t* samples, uint32_t numSamples, uint32_t frequency, bool loop);
void Sample_Stop();

#endif

