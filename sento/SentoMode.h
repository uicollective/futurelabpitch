#ifndef _SENTOMODE_H_
#define _SENTOMODE_H_

typedef class SentoMode {
  public:
  SentoMode() {};
  virtual void start() {};
  virtual void stop() {};
  virtual void tick(uint32_t ticks) {};
  virtual bool allowsModeChange() { return true; }
};

typedef class StartMode : public SentoMode {
  private: 
  bool done;
  public:
  StartMode();
  void start();
  void tick(uint32_t ticks);
  bool allowsModeChange();
};

typedef class IdleMode : public SentoMode { 
 public:
 IdleMode();
 virtual void start();
 virtual void tick(uint32_t ticks);
};

typedef class FactorChangeMode : public SentoMode {
  private:
  bool tempChange;
  bool brightChange;
  bool airChange;
  bool humidityChange;
  public:
  FactorChangeMode(bool tempChange, bool brightChange, bool airChange, bool humidityChange);
  virtual void start();
  virtual void tick(uint32_t ticks);
};

typedef class FactorCorrectionMode : public SentoMode {
  private:
  bool tempCause;
  bool brightCause;
  bool airCause;
  bool humidityCause;
  bool tempCorr;
  bool brightCorr;
  bool airCorr;
  bool humidityCorr;
  public:
  FactorCorrectionMode(bool inTempCause, bool inBrightCause, bool inAirCause, bool inHumidityCause, bool inTempCorr, bool inBrightCorr, bool inAirCorr, bool inHumidityCorr);
  virtual void start();
  virtual void tick(uint32_t ticks);
};

typedef class IssueResolvedMode : public SentoMode {
  public:
  IssueResolvedMode();
  void tick(uint32_t ticks);
};

typedef class ShutdownMode : public SentoMode {
  public:
  ShutdownMode();
  void start();
  void tick(uint32_t ticks);
};


#endif

