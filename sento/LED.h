#ifndef _LED_H_
#define _LED_H_

//--- RGBW neopixels

/* Note: All RGBW values here are in a perceptive linear color space (i.e. 128 means half visual brightness), so fades can be done
 * linearly. Gamma correction is applied in the very end when blitting RGB values to LEDs.
 */

/*! LED init */
void LED_Init();

/*! call once per tick */
void LED_Tick();

/*! sets the value of a specific RGBW-LED.
 * @param idx index of LED in ring (0..NUM_LEDS-1)
 * @param r,g,b,w r/g/b/w values (0-255)
 */
void LED_Set(uint8_t idx, uint8_t r, uint8_t g, uint8_t b, uint8_t w);

/*! sets all LEDs to a specific color
 * @param r,g,b,w r/g/b/w values (0-255)
 */
void LED_SetAll(uint8_t r, uint8_t g, uint8_t b, uint8_t w);

/*! fade a LED from current color to a target color in a given number of ticks 
 * @param idx index of LED in ring (0..NUM_LEDS-1)
 * @param r,g,b,w r/g/b/w values (0-255)
 * @param animation duration in ticks
*/
void LED_Fade(uint8_t idx, uint8_t r, uint8_t g, uint8_t b, uint8_t w, uint8_t ticks);


/*! fade all LEDs from current color to a target color in a given number of ticks 
 * @param r,g,b,w r/g/b/w values (0-255)
 * @param animation duration in ticks
*/
void LED_FadeAll(uint8_t r, uint8_t g, uint8_t b, uint8_t w, uint8_t ticks);

#endif

