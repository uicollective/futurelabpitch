#include "LED.h"
#include "constants.h"

Adafruit_NeoPixel led = Adafruit_NeoPixel(NUM_LEDS, NEO_RING_PIN, NEO_GRBW + NEO_KHZ800); 

uint8_t ledBuffer[NUM_LEDS][4];   // current RGBW values of LEDs

uint8_t ledFadeFrom[NUM_LEDS][4];  // fading: start RGBW values of LEDs
uint8_t ledFadeTo[NUM_LEDS][4];    // fading: end RGBW values of LEDs
uint8_t ledFadeDuration[NUM_LEDS]; // fading: animation duration in ticks (0 if not fading)
uint8_t ledFadeTime[NUM_LEDS];     // fading: current time of animation in ticks (0..ledFadeDuration)

void LED_Init() {
  led.begin(); 
  led.show();   // switching all off
}

void LED_Tick() {
  // service LEDs - perform fading, copy to LED stripe
  for(int i = 0; i<NUM_LEDS; i++){
    //check fading
    uint8_t duration = ledFadeDuration[i];
    if (duration) {
      if (i == 0) { Serial.println("Fading..."); }
      uint8_t fadeTime = ledFadeTime[i];
      if (fadeTime > duration) {
        fadeTime = duration;
      }
      for (uint8_t comp = 0; comp < 4; comp++) {  //do linear interpolation for r,g,b,w
        uint16_t mix = (ledFadeTo[i][comp] * fadeTime + (duration-fadeTime) * ledFadeFrom[i][comp]);
        ledBuffer[i][comp] = mix / duration;
      }
      if (fadeTime >= duration) { //check animation end
        if (i == 0) { Serial.println("Fading end"); }
        ledFadeDuration[i] = 0;
      } else {
        ledFadeTime[i] = fadeTime + 1;
      }
    }
    led.setPixelColor(i, led_gamma[ledBuffer[i][0]], led_gamma[ledBuffer[i][1]], led_gamma[ledBuffer[i][2]], led_gamma[ledBuffer[i][3]]); 
//    led.setPixelColor(i, ledBuffer[i][0], ledBuffer[i][1], ledBuffer[i][2], ledBuffer[i][3]); 
  }
  led.show(); //actually flush to LEDs
} 


void LED_Set(uint8_t idx, uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
   ledBuffer[idx][0] = r; 
   ledBuffer[idx][1] = g;
   ledBuffer[idx][2] = b;
   ledBuffer[idx][3] = w;
   ledFadeTime[idx] = 0;  //stop animation if running
}

void LED_SetAll(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
  for (uint8_t i=0; i<NUM_LEDS; i++) {
    LED_Set(i,r,g,b,w);
  }
}

void LED_Fade(uint8_t idx, uint8_t r, uint8_t g, uint8_t b, uint8_t w, uint8_t ticks) {
  if (ticks < 1) {  //no animation at all
    LED_Set(idx, r, g, b, w);
  } else {
    ledFadeFrom[idx][0] = ledBuffer[idx][0];  //remember current color as start color
    ledFadeFrom[idx][1] = ledBuffer[idx][1];
    ledFadeFrom[idx][2] = ledBuffer[idx][2];
    ledFadeFrom[idx][3] = ledBuffer[idx][3];
    ledFadeTo[idx][0] = r;
    ledFadeTo[idx][1] = g;
    ledFadeTo[idx][2] = b;
    ledFadeTo[idx][3] = w;
    ledFadeDuration[idx] = ticks;
    ledFadeTime[idx] = 0;
  }
}

void LED_FadeAll(uint8_t r, uint8_t g, uint8_t b, uint8_t w, uint8_t ticks) {
  for (uint8_t i=0; i<NUM_LEDS; i++) {
    LED_Fade(i, r, g, b, w, ticks);
  }
}
