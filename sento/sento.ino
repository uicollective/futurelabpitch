#include <Adafruit_NeoPixel.h>
#include <SPI.h>
#include <math.h>
#include "constants.h"

#include "LED.h"
#include "Icons.h"
#include "BLE.h"
#include "LED.h"
#include "Icons.h"
#include "Proximity.h"

#include "SentoMode.h"

#define TICK_MS 20
#define NUM_LEDS 24
#define NUM_ICONS 4

#define NEO_RING_PIN 10
#define NEO_ICONS_PIN 12
#define DEBUG_LED_PIN 13


uint8_t const ICON_HIGHLIGHT[] = {123, 192, 191};
uint8_t const ICON_WHITE[] = {255, 230, 160};

uint8_t const RING_HIGHLIGHT[] = {112,199,237, 0};
uint8_t const RING_WHITE[] = {0,0,0,160};


//--- Speaker (dummy for now)
void Speaker_Init();
void Speaker_Tick();

//--- Display (dummy, most likely dropped)
void Display_Init();

//main prototypes
void tick();
void setMode(SentoMode* newMode);

//globals
SentoMode* currentMode = NULL;
static uint32_t modeTicks = 0;
StartMode startMode = StartMode();
IdleMode idleMode = IdleMode();
ShutdownMode shutdownMode = ShutdownMode();
FactorChangeMode tempChangeMode = FactorChangeMode(false, false, false, true);
FactorChangeMode openWindowMode(true, false, false, true);
//FactorCorrectionMode openWindowMode(true, false, true, false, false, false, true, false);
FactorChangeMode duskDawnMode(false, true, false, false);
//FactorCorrectionMode duskDawnMode(false, true, false, false, true, false, true, false);
FactorChangeMode guestAtHomeMode(true, false, true, true);
IssueResolvedMode issueResolvedMode = IssueResolvedMode();

void setup() {
  Serial.begin(115200);
  Serial.println("Initializing..."); 
  pinMode(DEBUG_LED_PIN, OUTPUT);
  
  LED_Init();
  Speaker_Init();
  Icons_Init();
  BLE_Init();
  Display_Init();
  Proxy_Init();
  
  Serial.println("done!"); 
  
  setMode(&startMode);
}

void loop() {
  static uint32_t lastMillis = 0;  
  uint32_t nowMillis = millis();
  if (nowMillis - lastMillis >= TICK_MS) {
    tick();
    lastMillis = nowMillis;
  }
}

/*! called every TICK_MS milliseconds */
void tick() {
  //1. read sensors
  // Proxy_Tick();
  BLEEvent evt = BLE_Tick();
  
  //2. mode change?
  switch (evt) {
    case BLEEvent_Idle:
      Serial.println("goto idle");
      setMode(&idleMode);
      break;
    case BLEEvent_TempChange:
      Serial.println("goto temp change");
      setMode(&tempChangeMode);
      break;
    case BLEEvent_OpenWindow:
      Serial.println("goto open window");
      setMode(&openWindowMode);
      break;
    case BLEEvent_DuskDawn:
      //Scenario Evening / Morning
      Serial.println("goto dawn");
      setMode(&duskDawnMode);
      break;
    case BLEEvent_GuestAtHome:
      //Scenario Guests at Home
      Serial.println("goto game evening");
      setMode(&guestAtHomeMode);
      break;
    case BLEEvent_Resolved:
      Serial.println("goto issue resolved");
      setMode(&issueResolvedMode);
      break;
    case BLEEvent_OnOff:
      if (currentMode == &shutdownMode) {
        setMode(&startMode);
      } else {
        setMode(&shutdownMode);
      }
      break;
  }
    
  //3. let behaviour do its thing
  if (currentMode) {
    currentMode->tick(modeTicks);
  }

  //4. set outputs
  LED_Tick();
  Icons_Tick();
  Speaker_Tick();
  Display_Tick();
  
  //5. other housekeeping
  modeTicks++;
  digitalWrite(DEBUG_LED_PIN, modeTicks % 50 < 25);

  uint16_t swipe = Proxy_IsSwipe();
  if (swipe) {
    char buf[20];
    sprintf(buf,"swipe at %i",swipe);
    Serial.println(buf);
  }

}

void setMode(SentoMode* newMode) {
  if (currentMode == newMode) {
    return;
  }
  if (currentMode && !(currentMode->allowsModeChange())) {
    return;
  }

  if (currentMode) {
    Serial.println("Stopping current mode.");
    currentMode->stop();
  }
  currentMode = newMode;
  modeTicks = 0;
  if (currentMode) {
    Serial.println("Starting new Mode.");
    currentMode->start();
  }
}

//--- DRIVER CODE (todo: move to separate file or delete) ---

void Speaker_Init() {
  //todo
}
void Speaker_Tick() {
  //todo
}

void Display_Init() {
  //todo
}
void Display_Tick() {
  //todo
}

