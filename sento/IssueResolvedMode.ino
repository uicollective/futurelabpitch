#include "SentoMode.h"
#include "constants.h"

#define TICKS_PER_STEP 20
#define FADE_DURATION 10

IssueResolvedMode::IssueResolvedMode() {}

void IssueResolvedMode::tick(uint32_t ticks) {
  uint8_t currentStep = ticks / TICKS_PER_STEP;
  uint8_t ticksInStep = ticks % TICKS_PER_STEP;

  if (ticksInStep == 1) {
    switch (currentStep) {
      case 0:
        LED_FadeAll(RING_HIGHLIGHT[0], RING_HIGHLIGHT[1], RING_HIGHLIGHT[2], RING_HIGHLIGHT[3], TICKS_PER_STEP);
        Icons_Fade(Icon_Temperature, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], TICKS_PER_STEP);
        Icons_Fade(Icon_AirQuality,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], TICKS_PER_STEP);
        Icons_Fade(Icon_Humidity,    ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], TICKS_PER_STEP);
        Icons_Fade(Icon_Brightness,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], TICKS_PER_STEP);
        break;
      case 4:
        Icons_Fade(Icon_AirQuality,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        Icons_Fade(Icon_Brightness, 0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Humidity, 0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Temperature, 0, 0, 0, FADE_DURATION);
        break;
      case 5:
        Icons_Fade(Icon_AirQuality,  0,0,0, FADE_DURATION);
        Icons_Fade(Icon_Brightness, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 6:
        Icons_Fade(Icon_Brightness,  0,0,0, FADE_DURATION);
        Icons_Fade(Icon_Humidity, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 7:
        Icons_Fade(Icon_Humidity,  0,0,0, FADE_DURATION);
        Icons_Fade(Icon_Temperature, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 8:
        Icons_Fade(Icon_AirQuality,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        Icons_Fade(Icon_Brightness, 0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Humidity, 0, 0, 0, FADE_DURATION);
        Icons_Fade(Icon_Temperature, 0, 0, 0, FADE_DURATION);
        break;
      case 9:
        Icons_Fade(Icon_AirQuality,  0,0,0, FADE_DURATION);
        Icons_Fade(Icon_Brightness, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 10:
        Icons_Fade(Icon_Brightness,  0,0,0, FADE_DURATION);
        Icons_Fade(Icon_Humidity, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 11:
        Icons_Fade(Icon_Humidity,  0,0,0, FADE_DURATION);
        Icons_Fade(Icon_Temperature, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], FADE_DURATION);
        break;
      case 12:
        LED_FadeAll(RING_HIGHLIGHT[0], RING_HIGHLIGHT[1], RING_HIGHLIGHT[2], RING_HIGHLIGHT[3], 80);
        Icons_Fade(Icon_Temperature, ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], 80);
        Icons_Fade(Icon_AirQuality,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], 80);
        Icons_Fade(Icon_Humidity,    ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], 80);
        Icons_Fade(Icon_Brightness,  ICON_HIGHLIGHT[0], ICON_HIGHLIGHT[1], ICON_HIGHLIGHT[2], 80);
        break;
      case 20:
        setMode(&idleMode);
        break;
    }
  }
}

