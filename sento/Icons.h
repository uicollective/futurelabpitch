#ifndef _ICONS_H_
#define _ICONS_H_

//--- Touch button icons (RGB neopixels)

//white point that roughly matches the LED ring white point
#define ICON_WHITE_R 255
#define ICON_WHITE_G 230
#define ICON_WHITE_B 160

typedef enum _Icon {
  Icon_AirQuality = 0, 
  Icon_Brightness,
  Icon_Humidity,
  Icon_Temperature,
  Icon_Power
} Icon;
//typedef enum _Icon {
//  Icon_Humidity = 0, 
//  Icon_AirQuality,
//  Icon_Brightness,
//  Icon_Temperature,
//  Icon_Power
//} Icon;

void Icons_Init();
void Icons_Tick();

/*! sets the value of a specific LED.
 * @param icon icon to set
 * @param r,g,b r/g/b values (0-255)
 */
void Icons_Set(Icon icon, uint8_t r, uint8_t g, uint8_t b);

/*! fade an icon from current color to a target color in a given number of ticks 
 * @param icon icon to fade
 * @param r,g,b r/g/b values (0-255)
 * @param animation duration in ticks
*/
void Icons_Fade(Icon icon, uint8_t r, uint8_t g, uint8_t b, uint8_t ticks);

#endif
